<?php

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher as Dispatcher;
use Illuminate\Container\Container as Container;
use Illuminate\Routing\Router;

# define pathes
define('SRV_PATH', __DIR__);

# setup error logging
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('ignore_repeated_errors', TRUE);
ini_set('log_errors', TRUE);
ini_set('error_log', SRV_PATH.DIRECTORY_SEPARATOR.'logs/errors.log');

# get vendor loader
require 'vendor/autoload.php';

set_error_handler('Api\Components\ErrorComponent::handle', E_ALL);

# parse and cast configuration
$config = json_decode(
    json_encode(
        parse_ini_file(
            SRV_PATH . DIRECTORY_SEPARATOR . 'configuration' . DIRECTORY_SEPARATOR . 'configuration.ini'
        )
    )
);

# merge configuration with environment var
$_ENV = array_merge($_ENV, json_decode(json_encode($config), true));

# initialize global instances
$container  = new Container;
$dispatcher = new Dispatcher($container);
$resources  = new Router($dispatcher);

# build database connection
$capsule = new Capsule;
$capsule->setEventDispatcher($dispatcher);

$capsule->addConnection([
    'driver'    => 'mysql',
    'host'      => $config->database->hostname,
    'database'  => $config->database->database,
    'username'  => $config->database->username,
    'password'  => $config->database->password,
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
]);

$capsule->setAsGlobal();
$capsule->bootEloquent();

# require resources
require 'resources/common.php';

# handle resource routes
$request = Illuminate\Http\Request::createFromGlobals();

# dispatch routes
try {

    $response = $resources->dispatch($request);
    $response->send();

} catch (\Exception $exception) {

    error_log($exception->getMessage());

    http_response_code(500);
    header('Content-type:application/json');
    echo json_encode([
        'status'  => 500,
        'message' => $exception->getMessage()
    ]);
}
