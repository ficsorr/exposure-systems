<?php

namespace Api\Models;

use Illuminate\Database\Eloquent\Model;
use Api\Exceptions\DefaultException;
use Api\Helpers\StringHelper;

class Link extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'link';

    /**
     * Fillable properties
     * @var array
     */
    protected $fillable = ['link', 'route', 'uuid', 'status', 'expire', 'hash'];

    /**
     * Pending state
     * @var integer
     */
    const STATUS_PENDING = 0;

    /**
     * Clicked (used) state
     * @var integer
     */
    const STATUS_CLICKED = 1;

    /**
     * Booting model
     * @return null
     */
    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {

            if (empty(trim($model->route))) {
                throw new DefaultException('Invalid route to generate link');
            }

            $hash = StringHelper::createRandomHash(32);

            $model->link   = $model->link ? $model->link : ($model->route . '/' . $hash);
            $model->hash   = $hash;
            $model->status = $model->status ? $model->status : self::STATUS_PENDING;
        });
    }
}
