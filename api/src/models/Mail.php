<?php

namespace Api\Models;

use Illuminate\Database\Eloquent\Model;

class Mail extends Model
{
    protected $table = 'mail';

    protected $fillable = ['recepientName', 'recepientEmail', 'subject', 'schedule', 'status', 'body'];
}
