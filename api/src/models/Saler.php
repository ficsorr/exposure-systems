<?php

namespace Api\Models;

use Illuminate\Database\Eloquent\Model;
use Api\Exceptions\DefaultException;

class Saler extends Model
{
    protected $table = 'saler';

    protected $fillable = ['name','email','passh','priceListId','zohoId'];

    /**
     * Create password hash
     * @param  string $password1 // Password
     * @param  string $password2 // Password Repeated
     * @return null
     */
    public function createPasswordHash($password1, $password2)
    {
        # validate password
        if ((!trim($password1) || !trim($password2)) || (trim($password1) !== trim($password2))) {
            throw new DefaultException('Passwords are not match', 3);
        }

        # secure password properly
        $this->passh = hash(
            'sha256',
            hash('sha256', $_ENV['security']['hash']) . hash('sha256', trim($password1))
        );
    }

    /**
     * Compare password hash
     * @param  string $password
     * @return boolean
     */
    public function comparePasswordHash($password)
    {
        if (!$this->passh) {
            return false;
        }

        return $this->passh === hash(
            'sha256',
            hash('sha256', $_ENV['security']['hash']) . hash('sha256', trim($password))
        );
    }
}
