<?php

namespace Api\Components;

use Api\Exceptions\DefaultException;
use Api\Models\Link;

class LinkComponent
{
    /**
     * Add new link
     * All links are related to one of users
     * @param  string  $route // Route
     * @param  integer $uuid  // Unique User ID
     * @return string
     */
    public static function add($route, $uuid, $expire = 86400)
    {
        $entity = new Link();
        $entity->route  = trim($route);
        $entity->uuid   = (int) $uuid;
        $entity->expire = time() + $expire;
        $entity->save();

        return $entity->link;
    }

    public static function hash($hash)
    {
        return Link::firstWhere(["hash", '=', $hash], ['status', '=', Link::STATUS_PENDING]);
    }

    /**
     * Get link entity by link string
     * @param  string $link // Link to find
     * @return Shared\Models\UserLink
     * @throws DefaultException
     */
    public static function get($link)
    {
        $link   = trim(strip_tags($link));
        $entity = Link::firstWhere(["link", '=', $link], ['status', '=', Link::STATUS_PENDING]);

        if ($entity && time() > $entity->expire) {
            throw new DefaultException('Link is expired');
        }

        return $entity;
    }

    /**
     * Use link one time only
     * @param  string  $link // Link
     * @return boolean
     */
    public static function use($hash)
    {
        $link = self::hash($hash);

        if (!$link) {
            throw new DefaultException('Link not found');
        }

        $link->status = Link::STATUS_CLICKED;
        return $link->save();
    }
}
