<?php

namespace Api\Components;

class ErrorComponent
{
    public static function handle()
    {
        $error = error_get_last();

        header('Content-Type: application/json;  charset=UTF-8');

        if ($error) {
            echo json_encode([
                'message' => 'ERROR', 'rescode' => 500,
                'error'   => $error
            ]);
        }
    }
}
