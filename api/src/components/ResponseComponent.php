<?php

namespace Api\Components;

use Illuminate\Http\JsonResponse;

class ResponseComponent
{
    public static function create($code, $content = [])
    {
        $codemap = [
            200 => 'OK',
            201 => 'Created',
            202 => 'Accpeted',
            204 => 'No content',
            400 => 'Bad request',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not found',
            405 => 'Method not allowed',
            406 => 'Not acceptable',
            500 => 'Internal server error'
        ];
    
        $message  = empty($codemap[$code]) ? null : $codemap[$code];
    
        # prebuild response body
        $body = [];
        $body['rescode'] = $code;
        $body['message'] = $message;
    
        $body['dataset'] = $content;
    
        $response = new JsonResponse();
        $response->setStatusCode($code);
        $response->setContent(json_encode($body));
    
        return $response;
    }
}
