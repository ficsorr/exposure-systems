<?php

namespace Api\Components;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception as MailerException;

use Api\Models\Mail;
use Api\Exceptions\DefaultException;
use Illuminate\Database\Capsule\Manager as DB;

class MailComponent
{

    /**
     * Mail executor lock file path
     * @var string
     */
    protected static $lockpath = SRV_PATH . DIRECTORY_SEPARATOR . 'mail.lock';

    /**
     * Schedule mail
     * @param  integer  $uuid
     * @param  string   $subject
     * @param  string   $template
     * @param  array    $params
     * @param  integer  $schedule
     * 
     * @throws Exception
     * @return boolean
     */
    public static function schedule($email, $subject, $template, $params = [], $schedule = 0)
    {
        if ($schedule > 0 && $schedule <= time()) {
            throw new DefaultException('Its not delorean you cant travel in time');
        }

        $path = SRV_PATH . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'en-us';

        $file = $template . '.html';
        if (!file_exists($path)) {
            throw new DefaultException('Mail template file does not exists');
        }

        # render HTML content
        $loader   = new \Twig\Loader\FilesystemLoader($path);
        $simpview = new \Twig\Environment($loader, [
            'cache' => false
        ]);

        $contents = $simpview->render($file, $params);

        # create mail in pool
        $mail = new Mail([
            'recepientEmail'    => $email,
            'recepientName'     => 'partner',
            'subject'           => $subject,
            'schedule'          => $schedule,
            'body'              => $contents
        ]);

        return $mail->save();
    }

    /**
     * Is running locked
     * @return boolean
     */
    public static function locked()
    {
        return file_exists(self::$lockpath);
    }

    /**
     * Lock running
     * @return boolean
     */
    public static function lock()
    {
        return file_put_contents(self::$lockpath, time());
    }

    /**
     * Unlock running
     * @return boolean
     */
    public static function unlock()
    {
        return unlink(self::$lockpath);
    }

    /**
     * Execute mailing queue
     * @return boolean
     */
    public static function execute()
    {
        if (self::locked()) {
            return false;
        }

        self::lock();

        $thresshold = time();

        $prepared = "SELECT * FROM mail where (schedule = 0 or schedule = $thresshold)
        and status = 0 and tries < 3";

        $result = DB::connection()->select($prepared);

        foreach ($result as $row) {

            try {

                $mail = new PHPMailer(true);
                $mail->isSMTP();
                $mail->isHTML(true);
                $mail->SMTPAuth  = true;

                $mail->Host       = $_ENV['smtp']['hostname'];
                $mail->Username   = $_ENV['smtp']['username'];
                $mail->Password   = $_ENV['smtp']['password'];
                $mail->SMTPSecure = $_ENV['smtp']['encoding'];
                $mail->Port       = intval($_ENV['smtp']['portnumb']);

                $mail->setFrom($_ENV['smtp']['sendermail'], $_ENV['smtp']['sendername']);
                $mail->addAddress($row->recepientEmail, $row->recepientName);

                $mail->Subject = $row->subject;
                $mail->Body    = $row->body;

                $result = $mail->send();

                # update mail entry
                $entry = Mail::find($row->id);
                $entry->status = $result ? 1 : 0;
                $entry->tries  = $result ? $entry->tries : (int)$entry->tries += 1;
                $entry->save();
                
            } catch (MailerException $ex) {

                $entry = Mail::find($row->id);
                $entry->status = 0;
                $entry->tries  = (int)$entry->tries += 1;
                $entry->result = $ex->getMessage();
                $entry->save();
            }
        }

        self::unlock();

        return true;
    }
}
