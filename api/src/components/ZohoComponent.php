<?php

namespace Api\Components;

class ZohoComponent
{
    private $accessToken;
    private $config;
    private $uris;

    public function __construct()
    {
        # prepare configuration
        $this->config = json_decode(json_encode($_ENV['zoho']));

        # build uris
        $this->uris = (object) [
            'authenticate' =>
            'https://accounts.zoho.' .
                $this->config->region .
                '/oauth/v2/token',
            'api' => 'https://books.zoho.' . $this->config->region . '/api/v3/',
        ];
    }

    private function authenticate()
    {
        $handler = curl_init();
        curl_setopt($handler, CURLOPT_URL, $this->uris->authenticate);
        curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handler, CURLOPT_HEADER, false);

        $query = http_build_query([
            'refresh_token' => $this->config->refresh,
            'client_id' => $this->config->clientId,
            'client_secret' => $this->config->clientSc,
            'redirect_uri' => $this->config->redirect,
            'grant_type' => 'refresh_token',
        ]);

        curl_setopt($handler, CURLOPT_POST, 1);
        curl_setopt($handler, CURLOPT_POSTFIELDS, $query);

        $result = curl_exec($handler);

        if (curl_error($handler)) {
            trigger_error('Curl Error:' . curl_error($handler));
        }

        $decoded = json_decode($result);

        return $this->accessToken = $decoded->access_token;
    }

    private function request($uri, $method = 'GET', $data = [])
    {
        $this->authenticate();

        $uri = $this->uris->api . $uri;

        $handler = curl_init();
        curl_setopt($handler, CURLOPT_URL, $uri);
        curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handler, CURLOPT_HEADER, false);
        curl_setopt($handler, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Authorization: ' . 'Zoho-oauthtoken ' . $this->accessToken,
        ]);

        if (strtolower($method) == 'post') {
            curl_setopt($handler, CURLOPT_POST, 1);
            curl_setopt($handler, CURLOPT_POSTFIELDS, json_encode($data));
        }

        $result = curl_exec($handler);

        if (curl_error($handler)) {
            trigger_error('Curl Error:' . curl_error($handler));
        }

        return json_decode($result);
    }


    /**
     * Find wholesaler in Zoho database
     * @param string $email // E-mail address
     * @return mixed
     */
    public function getWholesaler($email)
    {
        $result = $this->request('contacts?email=' . $email);

        if (!empty($result->contacts)) {

            $contact = $this->request(
                'contacts/' . $result->contacts[0]->contact_id
            );

            if ($contact) {
                return $contact->contact;
            }
        }
    }

    /**
     * Get price list
     */
    public function getPriceList($priceBookId)
    {
        $result = $this->request('pricebooks/items?pricebook_id=' . $priceBookId);
        return $result;

        /*$result = $this->request('pricebooks/' . $priceBookId);
        if ($result && $result->code == 0) {
            return $result->pricebook->pricebook_items;
        }
        return [];*/
    }

    /**
     * Create an estimate for reseller
     */
    public function createEstimate($data = [])
    {
        return $this->request('estimates/?organization_id=' . $this->config->organize, 'POST', $data);
    }

    public function sendEstimate($estimateId, $data = [])
    {
        return $this->request('estimates/'.$estimateId.'/email?organization_id=' . $this->config->organize, 'POST', $data);
    }
}
