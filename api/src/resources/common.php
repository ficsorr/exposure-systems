<?php

use Api\Components\LinkComponent;
use Api\Components\MailComponent;
use Illuminate\Http\Request;
use Api\Components\ResponseComponent;
use Api\Components\ZohoComponent;
use Api\Models\Saler;

# Get settings and price list for default customer
$resources->get('/prices', function (Request $request) {

    $component = new ZohoComponent();
    $elements  = $component->getPriceList($_ENV['zoho']['pricelist']);

    return ResponseComponent::create(200, $elements);
});

# Get settings and price list for wholesaler
$resources->get('/prices/{id}', function (Request $request, $id) {

    $component = new ZohoComponent();
    $elements = $component->getPriceList($request->id);

    return ResponseComponent::create(200, $elements);
});

# Wholesaler login
$resources->post('/sign-in', function (Request $request) {

    $data = $request->json()->all();
    $email = $data['email'];
    $passw = $data['passw'];

    $saler = Saler::where('email', $email)->first();
    if (empty($saler)) {
        return ResponseComponent::create(404, 'Invalid login creditentals');
    }

    if (!$saler->comparePasswordHash($passw)) {
        return ResponseComponent::create(404, 'Invalid login creditentals');
    }

    return ResponseComponent::create(201, $saler);
});

# Confirm sign-up request
$resources->post('/sign-up-confirm', function (Request $request) {

    $data = $request->json()->all();
    $hash = empty($data['hash']) ? null : $data['hash'];
    $pass1 = empty($data['pass1']) ? null : $data['pass1'];
    $pass2 = empty($data['pass2']) ? null : $data['pass2'];

    if (!$hash || !$pass1 || $pass2) {
        return ResponseComponent::create(500, 'Invalid incoming data');
    }

    # find link
    $link = LinkComponent::hash($hash);
    if (empty($link)) {
        return ResponseComponent::create(404, 'Invalid hash or outdated');
    }

    # find saler
    $saler = Saler::where('zohoId', $link->uuid)->first();
    if (empty($saler)) {
        return ResponseComponent::create(404, 'Account not found');
    }

    # store password of saler
    $saler->createPasswordHash($pass1, $pass2);
    $saler->save();

    # set link to used
    LinkComponent::use($hash);

    return ResponseComponent::create(201);
});

# Sign up as a wholesaler
$resources->post('/sign-up', function (Request $request) {

    $data = $request->json()->all();

    # validate incoming data
    if (
        empty($data['email']) ||
        !filter_var($data['email'], FILTER_VALIDATE_EMAIL)
    ) {
        return ResponseComponent::create(500, 'Invalid e-mail address');
    }

    # find wholesaler
    $saler = Saler::where('email', $data['email'])->first();
    if (empty($saler)) {
        return ResponseComponent::create(403, 'Access denied');
    }

    # avoid resignup
    if ($saler->status == 1) {
        return ResponseComponent::create(400, 'Already signed up');
    }

    # generate link
    $link = LinkComponent::add('/', $saler->zohoId);

    # schedule email to wholesaler
    MailComponent::schedule(
        $data['email'],
        'Confirm registration',
        'confirm-registration',
        ['link' => $link]
    );

    return ResponseComponent::create(201);
});
