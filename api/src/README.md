## Setup instructions
---
**Host létrehozása**

nano /etc/hosts fájlban
add hozzá a végéhez a következő sort

127.0.0.1 exposure.api.localhost

---

**Docker image build**

docker-compose build
docker-compose up -d

---

**Adatbázis**

a database mappában található sql dump fájlt
szinkronizáld a saját lokál mysql server-en.