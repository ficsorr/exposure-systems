<?php

namespace Api\Helpers;

class StringHelper
{
    /**
     * Create random hash string
     * Length is limited to 128 characters because of SHA512 algorithm
     * @param  integer $length
     * @return string
     */
    public static function createRandomHash($length = 32)
    {
        $length = (int)$length <= 0 ? 32 : (int)$length;
        return substr(hash('SHA512', openssl_random_pseudo_bytes(20)), -$length);
    }
}
