<?php

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher as Dispatcher;
use Illuminate\Container\Container as Container;
use Api\Components\MailComponent;

# define pathes
define('SRV_PATH', __DIR__);

# setup error logging
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('ignore_repeated_errors', TRUE);
ini_set('log_errors', TRUE);
ini_set('error_log', SRV_PATH . DIRECTORY_SEPARATOR . 'logs/cron-errors.log');

# get vendor loader
require 'vendor/autoload.php';

# parse and cast configuration
$config = json_decode(
    json_encode(
        parse_ini_file(
            SRV_PATH . DIRECTORY_SEPARATOR . 'configuration' . DIRECTORY_SEPARATOR . 'configuration.ini'
        )
    )
);

# merge configuration with environment var
$_ENV = array_merge($_ENV, json_decode(json_encode($config), true));

# initialize global instances
$container  = new Container;
$dispatcher = new Dispatcher($container);

# build database connection
$capsule = new Capsule;
$capsule->setEventDispatcher($dispatcher);

$capsule->addConnection([
    'driver'    => 'mysql',
    'host'      => $config->database->hostmigr,
    'database'  => $config->database->database,
    'username'  => $config->database->username,
    'password'  => $config->database->password,
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
]);

$capsule->setAsGlobal();
$capsule->bootEloquent();

# __________________________________ #
# __________EXECUTE JOBS____________ #
# __________________________________ #

# Execute mail sending jobs 
MailComponent::execute();
