FROM ubuntu:latest

ENV TZ=Europe/Budapest
ENV DEBIAN_FRONTEND=noninteractive

EXPOSE 80
EXPOSE 443

RUN apt-get update

RUN apt-get install curl -y
RUN apt-get install git -y
RUN apt-get install zip -y
RUN apt-get install nano -y

RUN apt-get install apache2 -y

COPY ./server/conf/000-default.conf /etc/apache2/sites-available/000-default.conf
RUN chown -R www-data:www-data /var/www/html
RUN chmod o+w /var/www/

RUN apache2ctl configtest

RUN a2enmod rewrite
RUN a2enmod headers
RUN a2enmod expires

RUN apt-get update

RUN apt-get install -y software-properties-common

# install php
RUN add-apt-repository -y ppa:ondrej/php
RUN apt-get update
RUN apt install -y php7.3
RUN apt install -y libapache2-mod-php7.3
RUN apt install -y php7.3-mysql
RUN apt install -y php7.3-xml
RUN apt install -y php7.3-curl
RUN apt install -y php7.3-mbstring
RUN apt install -y php7.3-zip
RUN apt install -y php7.3-pdo

CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
