import config from '../src/config.json';
import office from '../src/assets/img/backgrounds/office.jpg'
import whitewall from '../src/assets/img/backgrounds/whitewall.jpg'


let store = {

    data: {

        // This controls which dropdowns are shown
        showheight: true,
        showwidth: true,
        showbackground: true,
        showapplication: true,
        showledPosition: false,
        showshipment: true,
        showframetype: true,
        showceiling: false,
        showcablePosition: false,
        showfeet: false,
        showcolor: true,
        showprint: true,

        // This controls which option is shown in each dropdowns
        shipment: 1,

        quoteItems: {},
        frametype:0,
        application: 0,
        picUrl: '',
        background: 0,
        heightWidthType: false,
        height: 600,
        width: 800,
        maxHeight: config.config.maxHeight,
        maxWidth: config.config.maxWidth,
        minHeight: config.config.minHeight,
        minWidth: config.config.minWidth,
        color: 0,


    },
    getData: function () {
        return this.data
    },

    setData: async function (name, value) {
        let promise = new Promise((resolve, reject) => {
            Object.assign(this.getData(), { ...this.getData(), [name]: value })
            resolve(this.data);
        });

        let result = await promise; // wait until the promise resolves (*)
        return result
    }

}



export default store


