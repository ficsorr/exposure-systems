/*
/ This software was developed by Attila Meszaros
/ All rights are reserverd, unless otherwise stated
/ 2020 - m.attila@outlook.com - hello@maxking.com.au
*/

import Designer from './components/Designer'
import Dropdown from './components/Dropdown'
import '../src/css/app.css'
import config from './config'
import store from './Store'
import Textinput from './components/Textinput'
import Header from '../src/components/Header'
import Footer from '../src/components/Footer'
import Quote from './components/Quote'
import Swal from 'sweetalert2'
import Preview from './components/Preview'
import { elementReady } from './Ready'
import { documentReady } from './DocumentReady'


//let store = {}

/*
/ This is the main class, this renders the
/ app on frontend
/
*/
export default class App {

  /*
  / Configuration data comes
  / from src/config.json (development only)
  */
  constructor(config) {
    this.config = config

    // Create designer object
    this.designer = new Designer({

      classNameDesignerWindow: 'ex-designer',
      classNamePicture: 'ex-picture',
      classPictureFrame: 'ex-picture-frame'
    })

    this.quote = new Quote(this.designer)

    // Set a reference in store so anybody can access it
    store.setData('designerObj', this.designer)
  }

  /*
   / Logic for  hiding dropdowns
   */
  toggleDropdownsByLogic(name, value) {



    // We only show max size input container, if shipoment was selected (not pickup)
    if (name === 'shipment') {
      if (parseInt(value) === 1) {
        document.getElementById('maxSizeInputContainer').classList.add('closed')
        document.getElementById('maxSizeInputContainer').classList.remove('open')
      } else {
        document.getElementById('maxSizeInputContainer').classList.remove('closed');
        document.getElementById('maxSizeInputContainer').classList.add('open');
      }
    }

    if (name === 'application') {

      // Hide LED position dropdown if type with no LED is selected
      if (parseInt(value) >= 5 && parseInt(value) <= 8)
        store.setData('showledPosition', true)
      else
        store.setData('showledPosition', false)

      // Hide cable position dropdown if type with no LED is selected
      if (parseInt(value) >= 5 && parseInt(value) <= 8)
        store.setData('showcablePosition', true)
      else
        store.setData('showcablePosition', false)


      // Hide ceiling mounts if application is not 2 or 7 
      if (parseInt(value) === 2 || parseInt(value) === 7)
        store.setData('showceiling', true)
      else
        store.setData('showceiling', false)

      // Hide frame type if print only is selected
      if (parseInt(value) === 9)
        store.setData('showframetype', false)
      else
        store.setData('showframetype', true)

      // Hide feet/wheels if application is not 3 or 8 
      if (parseInt(value) === 3 || parseInt(value) === 8)
        store.setData('showfeet', true)
      else
        store.setData('showfeet', false)

    }
  }

  /*
  / This method handles changes from
  / user inputs, and stores chnages in an object
  */
  inputChange(name, value) {

    store.setData(name, value)
      .then(() => {

        // Show or hide dropdowns based on certain loigic
        this.toggleDropdownsByLogic(name, value)

        if (name === 'application') {
          this.updateDropdowns()
        }

        if (name === 'background') {
          store.getData().preview.changeBackground()
        }

        if (name === 'color') {
          this.designer.changeFrameColor()
        }

        if (name === 'height') {

          // height and width must be between the limits
          if (value > config.config.maxHeight || value < config.config.minHeight)
            Swal.fire({
              icon: 'warning',
              text: 'Height must be between ' + config.config.minHeight + ' and ' + config.config.maxHeight + ' mm'
            })
          else
            this.designer.resizePictureFrame()
        }

        if (name === 'width') {

          // height and width must be between the limits
          if (value > config.config.maxWidth || value < config.config.minWidth)
            Swal.fire({
              icon: 'warning',
              text: 'Width must be between ' + config.config.minWidth + ' and ' + config.config.maxWidth + ' mm'
            })
          else
            this.designer.resizePictureFrame()
        }
      })
  }

  /**
   * This calculates prices
   */
  calculatePrice() {
    console.log(store)
  }

  renderSizeInputs() {

    const dropdownsDiv = document.createElement('div')
    dropdownsDiv.id = 'dropdownsDiv'
    dropdownsDiv.classList.add('ex-two-col-input')

    /**
     * Height dropdown or textinput
     * based on user selection
     */
    let height;
    const heightDropdownDiv = document.createElement('div')

    var hArray = Array.from(Array(store.getData().maxHeight).keys())
    var wArray = Array.from(Array(store.getData().maxWidth).keys())

    if (store.getData().heightWidthType) {

      // If 'pre-set values' is selected
      height = new Dropdown({
        name: 'height',
        options: hArray.map(item => item >= store.getData().minHeight ? item : false)
          .filter(item => item),
        label: 'Height',
        selected: 0,
        symbol: ' mm'
      })
    } else {
      // Default is manual input
      height = new Textinput({
        name: config.sizeDropDowns.height.name,
        label: config.sizeDropDowns.height.label,
        type: config.sizeDropDowns.height.type,
        defaultValue: store.getData().height,
        decimalPlaces: config.sizeDropDowns.height.decimalplaces,
        tooltip: config.sizeDropDowns.height.tooltip
      })
    }

    heightDropdownDiv.appendChild(height.render())

    /**
     * Width dropdown or textinput
     * based on user selection
     */

    let width;
    const widthDropdownDiv = document.createElement('div')
    // Add width dropdown

    if (store.getData().heightWidthType) {

      // If 'pre-set values' is selected
      width = new Dropdown({
        name: 'width',
        options: wArray.map(item => item >= store.getData().minWidth ? item : false)
          .filter(item => item),
        label: 'Width',
        selected: 0,
        symbol: ' mm'
      })
    } else {

      // Default is manual input
      width = new Textinput({
        name: config.sizeDropDowns.width.name,
        label: config.sizeDropDowns.width.label,
        type: config.sizeDropDowns.width.type,
        defaultValue: store.getData().width,
        decimalPlaces: config.sizeDropDowns.width.decimalplaces,
        tooltip: config.sizeDropDowns.width.tooltip
      })
    }
    widthDropdownDiv.appendChild(width.render())

    dropdownsDiv.appendChild(heightDropdownDiv)
    dropdownsDiv.appendChild(widthDropdownDiv)

    return dropdownsDiv
  }

  /**
  * This updates the custom toggle text
  * under the size inputs
  */
  updateCustomToggle() {
    let text = store.getData().heightWidthType ? this.config.textContent.customValueText : this.config.textContent.preSetValueText
    let customToggleText = document.getElementById('custom-toggle-text')
    customToggleText.innerHTML = '<span id="custom-toggle-text" class="ex-link">' + text + '</span>'
  }

  /**
  * This updates the size inputs
  * when custom toggle is switched
  */
  updateSizeInputs() {
    let sizeInputDiv = document.getElementById('size-input-div')
    sizeInputDiv.innerHTML = ''
    sizeInputDiv.appendChild(this.renderSizeInputs())
  }

  createInfoDisplay() {
    let errorDisplay = document.createElement('div')
    errorDisplay.classList.add('errorDisplay')
    //errorDisplay.classList.add('hidden')
    errorDisplay.id = 'info-display'
    return errorDisplay
  }

  createErrorDisplay() {
    let errorDisplay = document.createElement('div')
    errorDisplay.classList.add('errorDisplay')
    //errorDisplay.classList.add('hidden')
    errorDisplay.id = 'error-display'
    return errorDisplay
  }

  createLoadDisplay() {
    let loadDisplay = document.createElement('div')
    let spinner = document.createElement('div')
    loadDisplay.appendChild(spinner)
    spinner.classList.add('spinner')
    loadDisplay.classList.add('loadDisplay')
    //errorDisplay.classList.add('hidden')
    loadDisplay.id = 'load-display'
    return loadDisplay
  }

  displayLoad(show) {
    let errorDisp = document.getElementById('load-display')
    if (show) {
      errorDisp.style.display = 'block'
    } else {
      errorDisp.style.display = 'none'
    }

  }

  displayInfo(error, show, element) {
    let errorDisp = document.getElementById(element)
    if (show) {
      errorDisp.style.display = 'block'
      errorDisp.innerHTML = error
    } else {
      errorDisp.style.display = 'none'
    }

  }

  loadPrices() {

    elementReady('#info-display').then(() => {

      // Show loading modal
      this.displayLoad(true)

      try {

        fetch(config.api.prices)
          .then(data => data.json())
          .then(data => {

            if (data.rescode === 200 || data.rescode === 201) {
              this.displayLoad(false)
              store.setData('priceListPublicUser', data.dataset.items).then(console.log(store.getData().priceListPublicUser))
            }
            else {
              this.displayLoad(false)
              this.displayInfo('Error while loading the data. Server resposne: ' + data.status + ' ' + data.message, true, 'error-display')
            }
          }).catch(error => {
            this.displayLoad(false)
            this.displayInfo('Error while loading the data. Server resposne: ' + error, true, 'error-display')
          })

      } catch (error) {
        this.displayLoad(false)
        this.displayInfo('Error while loading the data. Server resposne: ' + error, true, 'error-display')
      }

    })
  }

  loadPricesMock() {

    elementReady('#info-display').then(() => {

      // Show loading modal
      this.displayLoad(true)

      setTimeout(() => {
        this.displayLoad(false)
      }, 1000);
    })
  }

  renderDropdowns() {
    const dropdownDiv = document.createElement('div')
    dropdownDiv.classList.add('ex-inputrow')

    config.dropdowns.map((item, index) => {

      let dropdown;

      if (index === 4) {

        // This is shown if shipment was selected as "shipping"
        dropdown = new Textinput({
          name: item.name,
          label: item.label,
          type: item.type,
          defaultValue: item.defaultvalue,
          decimalPlaces: item.decimalplaces,
          tooltip: item.tooltip
        })

        // Otherwise, we render the drpdowns
      } else {

        // Let's get the selected product/application
        let selectedProduct = store.getData().application

        dropdown = new Dropdown({
          name: item.name,

          options:
            // If frame type , then we select from an array of arrays instead of a single array, based on the selected application
            index === 5 ? item.options[selectedProduct] :
              // This is for the textile print option
              index === 9 ? (selectedProduct >= 0 && selectedProduct <= 4 ? item.options[0] : item.options[1]) : item.options,

          label: item.label,
          pictures: item.pictures,
          selected: store.getData()[item.name],
          sendSelected: 'id',

          tooltips:
            // If frame type , then we select from an array of arrays instead of a single array, based on the selected application
            index === 5 ? item.tooltips[selectedProduct] :
              // This is for the textile print option
              index === 9 ? (selectedProduct >= 0 && selectedProduct <= 4 ? item.tooltips[0] : item.tooltips[1]) : item.tooltips,
          tooltip: item.tooltip,
          show: item.show
        })
      }

      dropdownDiv.appendChild(dropdown.render())

    })
    return dropdownDiv
  }

  updateDropdowns() {

    let dropdownsDiv2 = document.getElementById('dropdownsDiv2')
    dropdownsDiv2.innerHTML = ''
    dropdownsDiv2.appendChild(this.renderDropdowns())

  }

  render() {

    // App container
    const container = document.createElement('div');
    container.classList.add('ex-app')

    // Add header
    container.appendChild(Header)

    // Info dsiplay
    container.appendChild(this.createInfoDisplay())
    container.appendChild(this.createLoadDisplay())
    container.appendChild(this.createErrorDisplay())

    // Content area
    const content = document.createElement('div');
    content.classList.add('ex-content')
    container.appendChild(content)

    // Add 2 main columns
    for (let i = 0; i < this.config.columns; i++) {

      const column = document.createElement('div')
      column.id = 'column' + i
      column.classList.add('ex-column')

      // Place the designer and the file upload in the forst column
      if (i === 0) {

        // Render the designer
        column.appendChild(this.designer.render())

      }

      /**
      * Elements in second column
      *
      */
      if (i === 1) {

        let sizeInputDiv = document.createElement('div')
        sizeInputDiv.id = 'size-input-div'


        //let customToggleDiv = document.createElement('div')
        //customToggleDiv.id = 'custom-toggle'
        //customToggleDiv.classList.add('ex-custom-toggle')

        sizeInputDiv.appendChild(this.renderSizeInputs())

        // Size inputs (2 columns)
        column.appendChild(this.designer.createHeader('ex-header2', 'Frame options'))
        column.appendChild(sizeInputDiv)

        //column.appendChild(customToggleDiv)

        //let customToggleText =
        //  store.getData().heightWidthType === 0 ?
        //    this.config.textContent.customValueText : this.config.textContent.preSetValueText

        //customToggleDiv.innerHTML = '<span id="custom-toggle-text" class="ex-link">' + customToggleText + '</span>'
        //customToggleDiv.addEventListener('click', () => {

        //  store.setData('heightWidthType', !store.getData().heightWidthType)
        //    .then((store) => {
        //      console.log(store)
        //      this.updateSizeInputs()
        //       this.updateCustomToggle()
        //     })
        // })

        const dropdownsDiv2 = document.createElement('div')
        dropdownsDiv2.id = 'dropdownsDiv2'
        dropdownsDiv2.classList.add('ex-one-col-input')

        column.appendChild(dropdownsDiv2)

        dropdownsDiv2.appendChild(this.renderDropdowns())
        column.appendChild(this.quote.render())
      }

      // Add the columns
      content.appendChild(column)

      // Add footer
      container.appendChild(Footer)
    }

    //this.loadPrices()
    this.loadPricesMock()

    return container;
  }
}

const app = new App(config)

store.setData('app', app)
document.body.appendChild(app.render());




