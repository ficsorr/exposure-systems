import '../css/designer.css'
import store from '../Store'
import Swal from 'sweetalert2'
import config from '../config.json'
import { elementReady } from '../Ready'
import Cropper from 'cropperjs'
import 'cropperjs/dist/cropper.css';
import Autonumeric from 'autonumeric'
import Preview from './Preview'

class Designer {

    constructor({ classNameDesignerWindow, classNamePicture, classPictureFrame }) {
        this.config = {
            classNameDesignerWindow: classNameDesignerWindow,
            classNamePicture: classNamePicture,
            classPictureFrame: classPictureFrame
        }

       // console.log('design construcotr ')

    }

    createFileUpload() {
        // Add file upload

        let fileUpload = document.createElement('input')

        fileUpload.type = 'file'
        fileUpload.accept = "image/png, image/jpeg, image/tiff, image/gif, .pdf"
        fileUpload.classList.add('custom-file-input')

        fileUpload.onchange = (event) => {
            this.imgUpload(event)
        }

        return fileUpload
    }

    createToolbar() {

        let toolbar = document.createElement('div')
        toolbar.classList.add('ex-toolbar')

        // add Zoom buttons
        toolbar.appendChild(this.createRotateButtons())
        toolbar.appendChild(this.createDisplayValues())
        //toolbar.appendChild(this.createFileUpload())


        return toolbar
    }


    createToolbarTop() {

        let toolbarTop = document.createElement('div')
        toolbarTop.classList.add('ex-toolbar-top')

        // add values
        toolbarTop.appendChild(this.createFileUpload())

        let div = document.createElement('div')
        div.classList.add('ex-toolbar-right')

        div.appendChild(this.createGetButton())
        div.appendChild(this.createResetButton())

        toolbarTop.appendChild(div)

        return toolbarTop
    }

    createGetButton() {

        let get = document.createElement('button')
        get.classList.add('ex-mini-button')
        get.innerHTML = 'preview'

        get.addEventListener('click', () => {

            let designer = document.getElementById('design-container')
            let preview = document.getElementById('preview-container-main')

            //designer.classList.toggle('hidden', 'visible')

            console.log(preview.classList.contains('closed'))

            preview.classList.toggle('closedPreview')
            preview.classList.toggle('openPreview')

            /*
            if (preview.classList.contains('hidden'))
                preview.classList.remove('hidden')
            else
                preview.classList.add('hidden')
*/



            /*
                        store.getData().croppie.getCroppedCanvas().toBlob(function (blob) {
            
                            blobToBase64(blob).then(img => {
                                Swal.fire(
                                    {
                                        icon: 'success',
                                        title: 'Grat job, your canvas image is here!',
                                        imageUrl: img
            
                                    }
                                )
                                store.setData('cropped', img)
                            })
            
                        });
            */
            const blobToBase64 = blob => {
                const reader = new FileReader();
                reader.readAsDataURL(blob);
                return new Promise(resolve => {
                    reader.onloadend = () => {
                        resolve(reader.result);
                    };
                });
            };

        })

        return get
    }

    createDesignerContainer() {

        // Create designer container
        let designerContainer = document.createElement('div')
        designerContainer.id = 'design-container'

        //designerContainer.id = 'crop-div'
        designerContainer.classList.add('ex-image-container')

        let img = document.createElement('img')
        img.id = 'crop-img'

        img.classList.add('ex-uploaded-image')

        img.src = config.defaultPicture.url
        designerContainer.appendChild(img)




        return designerContainer
    }

    createDisplayValues() {

        let display = document.createElement('div')
        display.classList.add('display')
        let resDisplay = document.createElement('div')
        let zoomDisplay = document.createElement('div')
        resDisplay.id = 'res-display'
        zoomDisplay.id = 'scale-display'


        //display.appendChild(this.createFileUpload())
        display.appendChild(resDisplay)
        display.appendChild(zoomDisplay)
        return display

    }

    createResetButton() {


        let reset = document.createElement('button')
        reset.classList.add('ex-mini-button')
        reset.innerHTML = '<i class="fa fa-refresh" aria-hidden="true"></i>'
        reset.addEventListener('click', () => {
            store.getData().croppie.reset()

        })

        return reset
    }

    createRotateButtons() {

        let rotateDiv = document.createElement('div')
        rotateDiv.classList.add('ex-toolbar-right')

        let rotateLeft = document.createElement('button')
        let rotateRight = document.createElement('button')
        let decrease = document.createElement('button')
        let increase = document.createElement('button')
        let toLeft = document.createElement('button')
        let toRight = document.createElement('button')
        let toUp = document.createElement('button')
        let toDown = document.createElement('button')

        rotateLeft.classList.add('ex-mini-button')
        rotateRight.classList.add('ex-mini-button')
        decrease.classList.add('ex-mini-button')
        increase.classList.add('ex-mini-button')
        toLeft.classList.add('ex-mini-button')
        toRight.classList.add('ex-mini-button')
        toUp.classList.add('ex-mini-button')
        toDown.classList.add('ex-mini-button')

        rotateLeft.innerHTML = '<i class="fa fa-rotate-left"></i>'
        rotateRight.innerHTML = '<i class="fa fa-rotate-right"></i>'
        decrease.innerHTML = '<i class="fa fa-search-minus" aria-hidden="true"></i>'
        increase.innerHTML = '<i class="fa fa-search-plus" aria-hidden="true"></i>'
        toLeft.innerHTML = '<i class="fa fa-arrow-left" aria-hidden="true"></i>'
        toRight.innerHTML = '<i class="fa fa-arrow-right" aria-hidden="true"></i>'
        toUp.innerHTML = '<i class="fa fa-arrow-up" aria-hidden="true"></i>'
        toDown.innerHTML = '<i class="fa fa-arrow-down" aria-hidden="true"></i>'

        rotateLeft.addEventListener('click', () => {
            store.getData().croppie.rotate(-10)
        })

        rotateRight.addEventListener('click', () => {
            store.getData().croppie.rotate(10)
        })

        decrease.addEventListener('click', () => {
            let currentScaleX = store.getData().croppie.getData().scaleX
            let currentScaleY = store.getData().croppie.getData().scaleY
            store.getData().croppie.scale(currentScaleX - 0.05, currentScaleY - 0.05)
            document.getElementById('scale-display').innerHTML = 'zoom:' + (currentScaleY - 0.05).toFixed(2)
        })

        increase.addEventListener('click', () => {
            let currentScaleX = store.getData().croppie.getData().scaleX
            let currentScaleY = store.getData().croppie.getData().scaleY
            store.getData().croppie.scale(currentScaleX + 0.05, currentScaleY + 0.05)
            document.getElementById('scale-display').innerHTML = 'zoom:' + (currentScaleY + 0.05).toFixed(2)
        })

        toLeft.addEventListener('click', () => {

            let currentData = store.getData().croppie.getImageData()
            console.log(currentData)
            store.getData().croppie.move(- 1, 0)

        })

        toRight.addEventListener('click', () => {

            let currentData = store.getData().croppie.getImageData()
            console.log(currentData)
            store.getData().croppie.move(1, 0)

        })

        toUp.addEventListener('click', () => {

            let currentData = store.getData().croppie.getImageData()
            console.log(currentData)
            store.getData().croppie.move(0, - 1)

        })

        toDown.addEventListener('click', () => {

            let currentData = store.getData().croppie.getImageData()
            console.log(currentData)
            store.getData().croppie.move(0, 1)



        })

        rotateDiv.appendChild(toLeft)
        rotateDiv.appendChild(toRight)
        rotateDiv.appendChild(toUp)
        rotateDiv.appendChild(toDown)

        rotateDiv.appendChild(decrease)
        rotateDiv.appendChild(increase)

        rotateDiv.appendChild(rotateLeft)
        rotateDiv.appendChild(rotateRight)

        return rotateDiv
    }


    changeFrameColor() {

        let currentColor = config.dropdowns[2].options[store.getData().color]
        let pictureFrame = document.getElementById('preview');
        pictureFrame.style.borderColor = currentColor

    }



    renderCropper() {

        let that = this

        elementReady('#crop-img').then((el => {
            // let dpr = document.getElementById('crop-img').devicePixelRatio
            // console.log(dpr)

            //If we have a cropper instance, let's destroy it
            if (store.getData().croppie) {
                store.getData().croppie.destroy()
            }

            let multiplier = 1.5;


            // Now let's see the selected size by the user
            let selectedHeight = store.getData().height
            let selectedWidth = store.getData().width

            // Height and width input fields. We will set this on resize
            let heightInput = Autonumeric.getAutoNumericElement(document.getElementById('height'))
            let widthInput = Autonumeric.getAutoNumericElement(document.getElementById('width'))

            let croppie = new Cropper(el, {

                crop(event) {

                    // This is the actual size of the crop box on the screen. We 
                    let currentHeight = (event.detail.height * multiplier).toFixed(0)
                    let currentWidth = (event.detail.width * multiplier).toFixed(0)

                    if (currentHeight > config.config.maxHeight || currentWidth > config.config.maxWidth) {
                        store.getData().app.displayInfo('Info: Maximum size exceeded! Max height: ' + config.config.maxHeight + 'px, max width:' + config.config.maxWidth + 'px. Please keep the size within the limits.', true, 'info-display')

                    }
                    else {
                        store.getData().app.displayInfo('', false, 'info-display')
                        //Now set the height and width input fields
                        store.setData('height', currentHeight).then(() => heightInput.set(currentHeight))
                        store.setData('width', currentWidth).then(() => widthInput.set(currentWidth))
                    }
                },
                responsive: true,
                zoomable: false,
                cropBoxMovable: true,
                cropBoxResizable: true,
                dragMode: 'move',
                viewMode: 0,
                center: false,
                guides: false,
                preview: '#preview',


                ready() {


                    let containerWidth = this.cropper.getCanvasData().naturalWidth
                    let containerHeight = this.cropper.getCanvasData().naturalHeight

                    let cropBoxWidth = selectedWidth / multiplier
                    let cropBoxHeight = selectedHeight / multiplier

                    this.cropper.setData({

                        // Center the image 
                        x: containerWidth / 2 - cropBoxWidth + (cropBoxWidth / 2),
                        y: containerHeight / 2 - cropBoxHeight + (cropBoxHeight / 2),

                        width: selectedWidth / multiplier,
                        height: selectedHeight / multiplier,

                    })
                }


            });

            // Let's store the current croppie in the store
            store.setData('croppie', croppie)
        }))

    }

    resizePictureFrame() {

        // Resize picture frame
        this.renderCropper()
    }

    createHeader(cssClass, text) {

        // Header div
        let headerDiv = document.createElement('div')
        headerDiv.classList.add(cssClass)

        // Square
        let square = document.createElement('span')
        square.innerHTML = 'i'
        square.classList.add('ex-header-square')

        // Text
        let header = document.createTextNode(text)

        headerDiv.appendChild(square)
        headerDiv.appendChild(header)
        return headerDiv

    }


    render() {

        // Creaate Main designer container
        let designerContainerMain = document.createElement('div')

        // Add header
        designerContainerMain.appendChild(this.createHeader('ex-header2', 'Designer'))

        // Add designer container
        designerContainerMain.appendChild(this.createDesignerContainer())

        // Add toolbar
        designerContainerMain.appendChild(this.createToolbarTop())

        // Add toolbar
        designerContainerMain.appendChild(this.createToolbar())

        // Add preview
        let preview = new Preview();
        store.setData('preview', preview)
        designerContainerMain.appendChild(preview.render())

        this.renderCropper()

        return designerContainerMain

    }

    imgUpload(event) {

        // Assuming only image
        var file = event.target.files[0];
        console.log('f: ' + file)
        let img = new Image()
        let picRatio = 0

        img.src = window.URL.createObjectURL(event.target.files[0])

        if (file.size < config.config.maxFileSize) {
            img.onload = () => {
                console.log('img dim:' + img.width + " " + img.height);
                picRatio = (img.height - img.width) / (img.width)
                document.getElementById('res-display').innerHTML =

                    '<div class="fileInfoItem"><b>File name:</b> ' + file.name + '</div>\
                    <div class="fileInfoItem"><b>Image resolution:</b> ' + img.width + ' x ' + img.height + ' pixels</div>\
                <div class="fileInfoItem"><b>Image type:</b> '+ file.type + '</div>\
                <div class="fileInfoItem"><b>Image size: </b>'+ (file.size / 1000000).toFixed(2) + ' MB</div>'


                if (img.width > config.config.minImgWidth && img.height > config.config.minImgWidth) {
                    var reader = new FileReader();
                    var url = reader.readAsDataURL(file);
                    console.log(url) // Would see a path?


                    // Once the picture is loaded, save it to state, and replace the existing one
                    reader.onloadend = (e) => {
                        store.setData('picUrl', reader.result).then(() => {

                            store.getData().croppie.replace(reader.result, false)
                        })
                    };

                } else
                    Swal.fire({
                        icon: 'error',
                        text: 'Image dimensions must be at least ' + config.config.minImgHeight + '(h) x ' + config.config.minImgWidth + '(w) px'
                    })
            }

        } else
            Swal.fire({
                icon: 'error',
                text: 'File size must be under ' + config.config.maxFileSize / 1000000 + ' MB, and image size must be at least ' + config.config.minImgWidth + ' x ' + config.config.minImgHeight + ' px'
            })

    }

}

export default Designer