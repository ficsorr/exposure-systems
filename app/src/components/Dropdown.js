
import '../css/dropdown.css'
import App from '../index'
import tippy from 'tippy.js';
import 'tippy.js/dist/tippy.css'; // optional for styling
import store from '../Store';

/**
 * This class renders a dropdown with the options
 * passed to it. It also returns the selected value
 * through handleEvent() method
 */
class Dropdown {

    constructor(
        {
            name = 'dropdown',
            label = '',
            options = ['No available options'],
            selected = 0,
            symbol = '',
            sendSelected = 'value',
            pictures = [],
            tooltips = false, // Tooltiops for options
            tooltip = false,  // Tooltiop for dropdown
            show = true
        }) {

        // Config data
        this.config = {

            name: name, // Dropdown name
            label: label, // Label text
            options: options, // Options list
            selected: selected, // Currently selected item
            symbol: symbol,
            sendSelected: sendSelected, // What to send back to parent on click (id or text),
            pictures: pictures, // Option pictures (if any)
            tooltips: tooltips, // tooltips
            tooltip: tooltip, // Tooltip for dropdown
            show: show // Display or not
        }

        this.app = new App()
    }

    /**
    * This method calls app objects
    * change() method and passes the event value
    * @param event
    */
    toggleDropDown(event) {

        // determine window height
        let windowHeight = window.innerHeight
        let currentPosition = event.screenY

        // Max height of optons list
        let maxHeight = windowHeight - currentPosition

        // FInd the current options div
        let optionsDiv = document.getElementById(this.config.name + '-options')

        // Set the max height, based on current window height
        optionsDiv.style.maxHeight = maxHeight + 'px'

        // Toggle closed class
        optionsDiv.classList.toggle('closed')
        optionsDiv.classList.toggle('open')

        // this.app.inputChange(this.config.name, event.target.value)
    }

    /**
    * This method updates the selected option
    * @param event
    */
    optionSelected(event) {

        // send back selected data to parent component
        this.app.inputChange(
            this.config.name,

            this.config.sendSelected === 'id' ?
                event.target.id :
                this.config.sendSelected === 'value' ?
                    this.config.options[event.target.id] :
                    false
        )

        console.log(event.target.id)

        // lets close the options div
        let optionsDiv = document.getElementById(this.config.name + '-options')

        // Toggle closed class
        optionsDiv.classList.toggle('closed')
        optionsDiv.classList.toggle('open')

        // Now update the dropdown
        let dropdown = document.getElementById(this.config.name + 'dropdown')

        let selectedOption = this.config.options[event.target.id]
        console.log(selectedOption)
        dropdown.innerText = selectedOption + this.config.symbol
    }

    renderOptions() {

        const options = document.createElement('div')

        options.id = this.config.name + '-options'
        options.classList.add('ex-options')

        // Closed by default
        options.classList.add('closed')

        this.config.options ? this.config.options.map((item, index) => {

            const option = document.createElement('div')

            // If there are pictures set for the option
            if (this.config.pictures.length > 0) {

                // Create left and right column
                const optionInnerLeft = document.createElement('div')
                const optionInnerRight = document.createElement('div')

                // Put the option name in the right column
                optionInnerRight.appendChild(document.createTextNode(item + this.config.symbol))

                // Now put the picture in the left column
                let productImage = new Image()
                productImage.src = this.config.pictures[index]
                productImage.classList.add('ex-product-picture')
                productImage.id = index

                // On hover, show the product image in bigger
                // productImage.onmouseover = () => { showProductImagePopup() }
                // productImage.onmouseout = () => { hideProductImagePopup() }

                optionInnerLeft.appendChild(productImage)

                option.appendChild(optionInnerLeft)
                option.appendChild(optionInnerRight)
                optionInnerLeft.id = index
                optionInnerRight.id = index
                option.classList.add('ex-optionWithPicture')


                // If there are no pictures set for the option
            } else {
                option.classList.add('ex-option')
                option.appendChild(document.createTextNode(item + this.config.symbol))
            }

            // Add tootltips for options if any
            if (this.config.tooltips) {
                tippy(option, {
                    content: this.config.tooltips[index],
                    theme: 'material'
                });
            }



            option.id = index
            options.appendChild(option)

            option.addEventListener('click', (event) => this.optionSelected(event))


        }) : 'No data';

        return options
    }

    /**
    * This method renders the dropdown
    * unsing the options passed down in
    * constructor
    */
    render() {

        // Wrapper div for dropdown
        const dropdownContainer = document.createElement('div')
        dropdownContainer.id = this.config.name + 'Container'
        dropdownContainer.classList.add('ex-inputrow')

        // Add tootltip for dropdown if any
        if (this.config.tooltip) {
            tippy(dropdownContainer, {
                content: this.config.tooltip,
                theme: 'material'
            });
        }

        // Display or not? 
        let currentDropdown = 'show' + [this.config.name]
        console.log(currentDropdown)

        if (store.getData()[currentDropdown]){
            dropdownContainer.classList.replace('closed','open')
            store.setData(currentDropdown, true)
        } else {
            
            dropdownContainer.classList.remove('open')
            dropdownContainer.classList.add('closed')
            store.setData(currentDropdown, false)
        }
       

        // Wrapper div for label
        const label = document.createElement('div')
        label.classList.add('ex-label')

        // Get label text from config
        const labelText = document.createTextNode(this.config.label)
        label.appendChild(labelText)

        // Create dropdown
        const dropdown = document.createElement('div')
        dropdown.id = this.config.name + 'dropdown'

        // Get selected option from config
        let selectedOptionText = this.config.options[this.config.selected]
        const selectedOption = document.createTextNode(selectedOptionText + this.config.symbol)

        // Now show the selected option in dropdown
        dropdown.appendChild(selectedOption)

        // Event listener for toggling drowpdown
        dropdown.addEventListener('click', (event) => this.toggleDropDown(event))

        dropdown.classList = 'ex-dropdown'

        // Add label
        dropdownContainer.appendChild(label)

        // Add dropdown
        dropdownContainer.appendChild(dropdown)

        // Render options
        dropdownContainer.appendChild(this.renderOptions())

        return dropdownContainer
    }
}

export default Dropdown;