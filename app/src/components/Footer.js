
import config from '../config.json'
import Swal from 'sweetalert2'

let Footer = document.createElement('div');

Footer.classList.add('ex-footer')
let link = document.createElement('div')
link.innerHTML = 
link.innerHTML = '<div><span class="ex-link">' + config.textContent.footerLogintext + '</span></div>';
Footer.appendChild(link)
link.addEventListener('click', ()=>{

    Swal.fire({
        title: 'Enter you email address',
        text: 'Please enter the email address that is registered as a wholesale customer.',
        input: 'text',
        inputAttributes: {
          autocapitalize: 'off'
        },
        showCancelButton: true,
        confirmButtonText: 'Look up',
        showLoaderOnConfirm: true,
        preConfirm: (login) => {
          return fetch(`//api.github.com/users/${login}`)
            .then(response => {
              if (!response.ok) {
                throw new Error(response.statusText)
              }
              return response.json()
            })
            .catch(error => {
              Swal.showValidationMessage(
                `Request failed: ${error}`
              )
            })
        },
        allowOutsideClick: () => !Swal.isLoading()
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire({
            title: `${result.value.login}'s avatar`,
            imageUrl: result.value.avatar_url
          })
        }
      })
})
export default Footer;

