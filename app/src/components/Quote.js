

import store from '../Store'
import Swal from 'sweetalert2'
import config from '../config.json'
import '../../src/css/quote.css'
import Autonumeric from 'autonumeric'
import Designer from './Designer';

class Quote extends Designer {

    constructor({ classNameDesignerWindow, classNamePicture, classPictureFrame }) {
        super({ classNameDesignerWindow, classNamePicture, classPictureFrame })


        // Quote window window
        this.quoteDiv = document.createElement('div')
        this.quoteDiv.classList.add = 'ex-quote'

        // Amount container
        this.amountDiv = document.createElement('div')
        this.amountDiv.classList.add('ex-amount')

        // Autonumeric number formatting
        this.am = new Autonumeric(this.amountDiv, {
            currencySymbol: config.textContent.currencySymbol,
            currencySymbolPlacement: 's'
        })

        // Add to quote button
        this.addButtonDiv = document.createElement('div')
        this.addButtonDiv.classList.add('button-div')

        this.quantityDiv = document.createElement('div')
        this.buttonDiv = document.createElement('div')
        this.buttonDiv.classList.add('add-button-div')

        this.orderButtonDiv = document.createElement('div')
        this.orderButtonDiv.classList.add('order-button-div')

        // Add qty filed and button to div
        this.addButtonDiv.appendChild(this.quantityDiv)
        this.addButtonDiv.appendChild(this.buttonDiv)

        // add Button
        let button = document.createElement('button')
        button.innerHTML = 'Add to quote'
        button.classList.add('ex-button')
        button.classList.add('normal')
        button.addEventListener('click', this.addToQuote)

        // Order Button
        let orderButton = document.createElement('button')
        orderButton.innerHTML = 'PLACE ORDER'
        orderButton.classList.add('ex-button')
        orderButton.classList.add('large')
        orderButton.addEventListener('click', this.order)

        // Quantity input
        let qtyInput = document.createElement('input')
        qtyInput.classList.add('ex-qty-input')
        qtyInput.id = "qty-input"

        this.qty = new Autonumeric(qtyInput, {
            decimalPlaces: '0'
        })

        this.qty.set(1)

        // Quote items container
        this.quouteItemsContainer = document.createElement('div')
        this.quouteItemsContainer.id = 'ex-quote-items-container'

        // Now add elements to corresponding divs
        this.buttonDiv.appendChild(button)
        this.quantityDiv.appendChild(qtyInput)

        this.orderButtonDiv.appendChild(orderButton)

        // Order button is hidden by default
        this.orderButtonDiv.style.display = 'none'

        this.quoteDiv.appendChild(this.amountDiv)
        this.quoteDiv.appendChild(this.addButtonDiv)
        this.quoteDiv.appendChild(this.quouteItemsContainer)
        this.quoteDiv.appendChild(this.orderButtonDiv)


    }

    order() { }

    addToQuote() {

        // Rows config
        let quoteItems = store.getData().quoteItems
        let quoteItemsLength = Object.keys(quoteItems).length
        let currentRow = quoteItemsLength
        let currentAmount = 0
        let qty = Autonumeric.getAutoNumericElement(document.getElementById('qty-input')).getNumber()

        // Current Product config
        let height = store.getData().height
        let width = store.getData().width
        let color = config.dropdowns[2].options[store.getData().color]
        let frameType = config.dropdowns[1].options[store.getData().frametype]
        let application = config.dropdowns[0].options[store.getData().application]

        let productName = 'Frame ' + (currentRow + 1) + ' - ' + height + 'mm x ' + width + 'mm'
        let quoteItemsContainer = document.getElementById('ex-quote-items-container')

        // Add list headers
        if (currentRow === 0) {
            let headerText = '<div class="ex-quote-header">Quote</div>\
            <div class="ex-item-wrapper"><div class="ex-header-left" >Name</div>\
        <div>Qty</div>\
        <div>Amount</div>\
        </div><hr/>';
            //quoteItemsContainer.innerHTML = headerText;
            quoteItemsContainer.appendChild(super.createHeader('ex-header2','Quote'))
           

        }

        // Store current item in store
        store.setData('quoteItems', {

            ...quoteItems,
            [currentRow]: {
                productName: productName,
                qty: qty,
                color: color,
                height: height,
                width: width,
                price: 0
            }

        }).then(() => {
            console.log(store.getData())

            // Now display item
            let itemText = '<div id="row' + currentRow + '"><div class="ex-item-wrapper"><div class="ex-item" id=' + "itemrow" + currentRow + '>' + productName + '</div>\
            <div><input id="qty-input'+ currentRow + '" class="ex-qty-input" type="text" value="' + qty + '"/></div>\
            <div id="amount'+ currentRow + '">' + currentAmount + '</div>\
            </div>\
            <div class="ex-item-desc">Color: '+ color + ' </div>\
            <div class="ex-item-desc">Frame type: '+ frameType + ' </div>\
            <div class="ex-item-desc">Application: '+ application + ' </div>\
            <div id="ex-delete-link'+ currentRow + '" class="ex-link">delete item</div> <hr/></div>\
           ' 
            quoteItemsContainer.insertAdjacentHTML('beforeend', itemText)

            // Autonumeric number formatting
            this.am = new Autonumeric('#amount' + currentRow, {
                currencySymbol: config.textContent.currencySymbol,
                currencySymbolPlacement: 's'
            })

            // Autonumeric number formatting
            this.qt = new Autonumeric('#qty-input' + currentRow, {
                decimalPlaces: '0'
            })

            // Add order now button if there is minimum one item
            if (currentRow <= 0)
                document.querySelector('.order-button-div').style.display = 'block'

            // Event listener to update quantity in each quote rows
            document.getElementById('qty-input' + currentRow).addEventListener('keyup', (event) => {

                let currentItems = store.getData().quoteItems
                currentItems[currentRow].qty = event.target.value
                store.setData('quoteItems', currentItems).then(() => {
                    console.log(store.getData())
                })
            })

            // Event listener to delete a row
            document.getElementById('ex-delete-link' + currentRow).addEventListener('click', (event) => {


                document.getElementById('row' + currentRow).remove()

                let currentItems = store.getData().quoteItems
                delete currentItems[currentRow]
                store.setData('quoteItems', currentItems).then(() => {
                    console.log(store.getData())
                })
            })
        })
    }

    updatePrice() {
        let price = 0
        this.am.set(price)
    }

    render() {

        this.updatePrice()
        return this.quoteDiv
    }




}

export default Quote