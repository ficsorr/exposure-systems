import config from '../config.json'

let Header = document.createElement('div');

Header.classList.add('ex-header')
Header.innerHTML =
    '<span>' + config.textContent.header.split(" ")[0] + '</span>' +
    ' ' + '<span class="ex-orange">' + config.textContent.header.split(" ")[1] + '</span>';

export default Header;

