
import '../css/textinput.css'
import App from '../index'
import Autonumeric from 'autonumeric'
import tippy from 'tippy.js';
import 'tippy.js/dist/tippy.css'; // optional for styling
import store from '../Store';

/**
 * This class renders a dropdown with the options
 * passed to it. It also returns the selected value
 * through handleEvent() method
 */
class Textinput {

    constructor({ name, label, type, defaultValue, decimalPlaces, tooltip }) {
        // Config data
        this.config = {
            name: name,
            label: label,
            type: type,
            defaultValue: defaultValue,
            decimalPlaces: decimalPlaces,
            tooltip: tooltip
        }

        this.app = new App()
        this.el = null
    }

    /**
    * This method calls app objects
    * change() method and passes the event value
    * @param event
    */
    handleEvent(event) {
        this.app.inputChange(this.config.name, this.el.getNumber())
    }

    /**
    * This method renders the dropdown
    * unsing the options passed down in
    * constructor
    */
    render() {

        const textInputContainer = document.createElement('div')
        textInputContainer.id = this.config.name + 'Container'

         // Display or not? 
         let currentTextInput = 'show' + [this.config.name]
         console.log(currentTextInput)
 
         if (store.getData()[currentTextInput]){
            textInputContainer.classList.replace('closed','open')
             store.setData(currentTextInput, true)
         } else {
            textInputContainer.classList.add('closed')
            textInputContainer.classList.remove('open')
             store.setData(currentTextInput, false)
         }


        const label = document.createElement('div')
        label.classList.add('ex-label')
        const labelText = document.createTextNode(this.config.label)
        label.appendChild(labelText)

        // Create dropdown
        const textInput = document.createElement('input')

        // If number

        if (this.config.type === 'number') {
            this.el = new Autonumeric(textInput, {
                currencySymbol: ' mm',
                currencySymbolPlacement: 's',
                decimalplaces: this.config.decimalPlaces

            })
            this.el.set(this.config.defaultValue)
        }

        // Listen for events
        textInput.addEventListener('change', (event) => this.handleEvent(event))

        textInput.classList = 'ex-input'
        textInput.id = this.config.name

        // Add tootltip 
        if (this.config.tooltip) {
            tippy(textInput, {
                content: this.config.tooltip,
                theme: 'material'
            });
        }

        textInputContainer.appendChild(label)
        textInputContainer.appendChild(textInput)

        return textInputContainer
    }
}

export default Textinput;