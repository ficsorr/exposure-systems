import '../css/designer.css'
import store from '../Store'
import config from '../config.json'
import Dropdown from './Dropdown'
import 'cropperjs/dist/cropper.css';
import Designer from './Designer';

class Preview {

    createBgSelector() {

        // Add background selector
        const bgSelector = new Dropdown({
            name: 'background',
            options: config.backgrounds.map(item => item.name),
            selected: store.getData().background,
            sendSelected: 'id'
        })


        const bgDropdownDiv = document.createElement('div')
        bgDropdownDiv.appendChild(bgSelector.render())

        return bgDropdownDiv

    }

    createPreview() {

        let previewContainer = document.createElement('div')
        previewContainer.id = 'preview-container'
        previewContainer.classList.add('previewContainer')
        //previewContainer.classList.add('hidden')
        previewContainer.style.backgroundImage = config.backgrounds[0].url
        previewContainer.style.backgroundSize = '100% 100%'

        let preview = document.createElement('div')
        preview.id = 'preview'
        preview.classList.add('preview')
        

        previewContainer.appendChild(preview)

        return previewContainer
    }

    changeBackground() {

        let currentBackground = store.getData().background
        console.log(store.getData().background)
        let previewContainer = document.getElementById('preview-container');

        previewContainer.style.backgroundImage = config.backgrounds[currentBackground].url
        previewContainer.style.backgroundSize = '100% 100%'

    }

    render() {

        // Creaate Main designer container
        let previewContainerMain = document.createElement('div')
        previewContainerMain.id='preview-container-main'
        previewContainerMain.classList.add('closedPreview')

        // Add header
        previewContainerMain.appendChild(store.getData().designerObj.createHeader('ex-header2','Preview'))

        // Add background selector to main ciontainer
        previewContainerMain.appendChild(this.createBgSelector())

        // Add preview
        previewContainerMain.appendChild(this.createPreview())

        return previewContainerMain

    }

}

export default Preview